const moment = require('moment')

export default class ValidationRules {
    rules = {

        // validation input has a value
        vrule_required: function (elm) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value
            //let val = elm.target.value;
                val = val.replace('£', '')

            if (val && val != 'Please select')
                return true
            else
                return { message: 'This question is required' }
        },

        // validation input has a value
        vrule_requiredIf: function (val, elm) {
            var queryElmVal = $(elm).val()

            if (queryElmVal > 0) {
                val = val.value;
                if (val)
                    return true
                else
                    return { message: 'This question is required' }
            } else {
                return true
            }

        },

        // ensure checkbox is ticked
        vrule_checkboxTrue: function (fieldset) {
            var checkbox = fieldset.querySelector('input[type=checkbox]')
            if (checkbox.checked)
                return true
            else
                return { message: 'Please tick this box to continue' }
        },

        // validation atleast one button selected
        vrule_btn_required: function (fieldset) {
            var buttons = fieldset.querySelectorAll('input[type=radio], input[type=checkbox]'),
                btnSelected = false

            for(let button of buttons){
                if (button.checked) {
                    btnSelected = true
                }
            };
            if (btnSelected)
                return true
            else
                return { message: 'Please select an option' }
        },

        // text input (letters and limited special characters)
        vrule_textInput: function (elm) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value
        
            let regex = /^[a-zA-Z'\-\s]+$/

            if (regex.test(val) || val == '')
                return true
            else
                return { message: 'Please only enter letters' }

        },

        // number input (numbers only)
        vrule_numberInput: function (val) {
            val = val.value,
                regex = /^\d+$/;

            if (regex.test(val))
                return true
            else
                return { message: 'Please only enter numbers' }
        },

        // validation input length is atleast (dynamic)
        vrule_minLength: function (elm, min) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value

            if (val.length >= min || val == '')
                return true
            else
                return { message: 'This field requires atleast ' + min + ' characters' }
        },
        vrule_maxLength: function (elm, max) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value

            if (val.length <= max || val == '')
                return true
            else
                return { message: 'This field can have no more than ' + min + ' characters' }
        },

        // minimum value of input
        vrule_minValue: function (val, min) {
            val = val.value
            val = val.replace('£', '').replace(/,/g, '')

            if (val >= min || val == '')
                return true
            else
                return { message: 'This field requires minimum value of ' + min }
        },

        // age range (between 21-65)
        vrule_ageRange: function (val, ageRange) {
            var dob = val,
                start = ageRange[0],
                end = ageRange[1],
                birthday = moment(dob, 'DD MM YYYY'),
                today = moment(),
                age = today.diff(birthday, 'years');

            if ((age >= start && age <= end) || isNaN(age) || dob == null)
                return true
            else
                return { message: 'You must be aged between ' + start + ' and ' + end };
        },


        // date must be provided in correct format
        vrule_date_required: function (val) {
            // val.length == 10 fits date format: DD_MM_YYYY
            if (val && val.length == 10)
                return true
            else
                return { message: 'This question is required' }
        },
        // valid date format (e.g. month cant be 15)
        vrule_date: function (val) {
            var date = val,
                validDate = moment(date, 'DD MM YYYY', true).isValid();

            if (validDate || date == null) { return true; } else {
                var invalidType = moment(date, 'DD MM YYYY', true).invalidAt();

                if (invalidType == -1) { // invalid year
                    return {
                        message: 'Please enter a valid date <br/>The <strong>year</strong> you entered is invalid',
                        type: 'year'
                    };
                } else if (invalidType == 1) { // invalid month
                    return {
                        message: 'Please enter a valid date <br/>The <strong>month</strong> you entered is invalid',
                        type: 'month'
                    };
                } else if (invalidType == 2) { // invalid day
                    return {
                        message: 'Please enter a valid date \<br/>\The \<strong>\day\</strong>\ you entered is invalid',
                        type: 'day'
                    };
                } else { // invalid general
                    return { message: 'Please enter a valid date' }
                }
            }
        },

        // validate that input date is not before X date
        vrule_notBeforeX: function (val, dateX) {
            var date = moment(val, 'DD MM YYYY'),
                dateX = moment(dateX, 'DD MM YYYY'),
                difference = date.diff(dateX, 'years');

            if (difference > 0)
                return true
        },

        // valid mobile number
        vrule_mobileNumber: function (elm) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value

            let phonenumber = val,
            starts07 = phonenumber.substring(0, 2) == '07',
            mobileLength = (phonenumber.length == 11);

            if ((starts07 && mobileLength) || phonenumber == "")
                return true
            else
                return { message: 'Please enter a valid mobile number' }
        },

        // valid home number
        vrule_homeNumber: function (elm) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value

            let phonenumber = val,
                homeNumberTest =
                    /^\(?0( *\d\)?){9,10}$/.test(phonenumber) &&
                    (phonenumber.substring(0, 2) == '01' || phonenumber.substring(0, 2) == '02')

            if (homeNumberTest || phonenumber == "")
                return true
            else
                return { message: 'Please enter a valid home phone number' }
        },

        // valid home or mobile number
        vrule_anyPhone: function (elm) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value
             
            let phonenumber = val,
                homeNumberTest =
                    /^\(?0( *\d\)?){9,10}$/.test(phonenumber) &&
                    (phonenumber.substring(0, 2) == '01' || phonenumber.substring(0, 2) == '02'),

                mobileNumberTest =
                    phonenumber.substring(0, 2) == '07' && phonenumber.length == 11 ||
                    phonenumber.substring(0, 4) == '+447' && phonenumber.length == 13 ||
                    phonenumber.substring(0, 3) == '447' && phonenumber.length == 12;

            if (homeNumberTest || mobileNumberTest)
                return true
            else
                return { message: 'Please provide a valid mobile or home phone number' }
        },

        // valid email address
        vrule_email: function (elm) {
            let val = elm.value;
            if(val == undefined)
                val = elm.target.value

            let email = val

            const emailRegex = /^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if (emailRegex.test(email) || email == "")
                return true
            else
                return { message: 'Please enter a valid email address' }
        },

        // currency format valid
        vrule_currency: function (val) {
            val = val.value,
                regex = /^[\d£.,]+$/;
            if (val) {
                if (regex.test(val))
                    return true
                else
                    return { message: 'This input can only contain numbers, a pound sign and commas e.g. £1,000' }
            } else {
                return true
            }
        },

        // postcode valid
        vrule_postcode: function (val) {
            var postcode = val.value,
                regex = /[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}/i;

            // min length 6, max length 8, matches postcode regex
            if (postcode.length >= 6 && postcode.length <= 8 && regex.test(postcode))
                return true
            else
                return { message: 'Please enter a valid postcode' }
        }

    }
}

/*
rules = {
    // validation input has a value
    vrule_required: function (val) {
        val = val.value;
        val = val.replace('£', '')
        if (val && val != 'Please select') { return true; }
        else {
            return { message: 'This question is required' };
        }
    },

    // validation input has a value
    vrule_requiredIf: function (val, elm) {
        var queryElmVal = $(elm).val()

        if (queryElmVal > 0) {
            val = val.value;
            if (val) { return true; }
            else {
                return { message: 'This question is required' }
            }
        } else {
            return true
        }

    },

    // ensure checkbox is ticked
    vrule_checkboxTrue: function (fieldset) {
        console.log(fieldset)
        var checkbox = fieldset.querySelector('input[type='checkbox']');
        console.log(checkbox)
        if (checkbox.checked) { return true; }
        else {
            if (checkbox.id == 'importantInfo') {
                // if the checkbox is the PL important info checkbox show specific message
                return { message: 'Please confirm that you have read the 'important information' above.' }
            } else {
                return { message: 'Please tick this box to continue' };
            }
        }
    },

    // validation atleast one button selected
    vrule_btn_required: function (fieldset) {
        var buttons = fieldset.querySelectorAll('input[type='radio'], input[type='checkbox']'),
            btnSelected = false

        Array.prototype.filter.call(buttons, function (button) {
            if (button.checked) {
                btnSelected = true
            }
        });
        if (btnSelected) { return true }
        else {
            return { message: 'Please select an option' }
        }
    },

    // text input (letters and limited special characters)
    vrule_textInput: function (val) {
        val = val.value,
            regex = /^[a-zA-Z'\-\s]+$/

        if (regex.test(val) || val == '') { return true; } else {
            return { message: 'Please only enter letters' }
        }
    },

    // number input (numbers only)
    vrule_numberInput: function (val) {
        val = val.value,
            regex = /^\d+$/;

        if (regex.test(val)) { return true; } else {
            return { message: 'Please only enter numbers' }
        }
    },

    // validation input length is atleast (dynamic)
    vrule_minLength: function (val, min) {
        val = val.value

        if (val.length >= min || val == '') { return true; } else {
            return { message: 'This field requires atleast ' + min + ' characters' }
        }
    },

    // minimum value of input
    vrule_minValue: function (val, min) {
        val = val.value
        val = val.replace('£', '').replace(/,/g, '')

        if (val >= min || val == '') { return true; } else {
            return { message: 'This field requires minimum value of ' + min }
        }
    },

    // age range (between 21-65)
    vrule_ageRange: function (val, ageRange) {
        var dob = val.value,
            start = ageRange[0],
            end = ageRange[1],
            birthday = moment(dob, 'DD MM YYYY'),
            today = moment(),
            age = today.diff(birthday, 'years');

        if (age >= start && age <= end) { return true; } else {
            return { message: 'You must be aged between ' + start + ' and ' + end };
        }
    },

    // valid date format (e.g. month cant be 15)
    vrule_date: function (val) {
        var date = val.value,
            validDate = moment(date, 'DD/MM/YYYY', true).isValid();

        if (validDate) { return true; } else {
            var invalidType = moment(date, 'DD/MM/YYYY', true).invalidAt();

            if (invalidType == 1) { // invalid month
                return {
                    message: 'Please enter a valid date <br/>The <strong>month</strong> you entered is invalid',
                    type: 'month'
                };
            } else if (invalidType == 2) { // invalid day
                return {
                    message: 'Please enter a valid date <br/>The <strong>day</strong> you entered is invalid',
                    type: 'day'
                };
            } else { // invalid general
                return { message: 'Please enter a valid date' }
            }
        }
    },

    // validate that input date is not before X date
    vrule_notBeforeX: function (val, dateX) {
        var date = moment(val, 'DD MM YYYY'),
            dateX = moment(dateX, 'DD MM YYYY'),
            difference = date.diff(dateX, 'years');

        console.log(difference)
        if (difference > 0) { return true }
    },

    // valid mobile number
    vrule_mobileNumber: function (val) {
        val = val.value,
            phonenumber = val,
            starts07 = phonenumber.substring(0, 2) == '07',
            mobileLength = (phonenumber.length == 11);

        if (starts07 && mobileLength) { return true; } else {
            return { message: 'Please enter a valid mobile number' }
        }
    },

    // valid home number
    vrule_homeNumber: function (val) {
        var phonenumber = val.value,
            homeNumber = /^\(?0( *\d\)?){9,10}$/.test(phonenumber);

        if (homeNumber) { return true; } else {
            return { message: 'Please provide a valid home phone number' }
        }
    },

    // valid home or mobile number
    vrule_anyPhone: function (val) {
        var phonenumber = val.value,
            homeNumberTest =
                /^\(?0( *\d\)?){9,10}$/.test(phonenumber) &&
                (phonenumber.substring(0, 2) == '01' || phonenumber.substring(0, 2) == '02'),

            mobileNumberTest =
                phonenumber.substring(0, 2) == '07' && phonenumber.length == 11 ||
                phonenumber.substring(0, 4) == '+447' && phonenumber.length == 13 ||
                phonenumber.substring(0, 3) == '447' && phonenumber.length == 12;

        if (homeNumberTest || mobileNumberTest) { return true; } else {
            return { message: 'Please provide a valid mobile or home phone number' }
        }
    },

    // valid email address
    vrule_email: function (val) {
        var emailRegex = /^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (emailRegex.test(val.value)) { return true; } else {
            return { message: 'Please enter a valid email address' }
        }
    },

    // currency format valid
    vrule_currency: function (val) {
        val = val.value,
            regex = /^[\d£.,]+$/;
        if (val) {
            if (regex.test(val)) { return true; } else {
                return { message: 'This input can only contain numbers, a pound sign and commas e.g. £1,000' }
            }
        } else {
            return true
        }
    },

    // postcode valid
    vrule_postcode: function (val) {
        var postcode = val.value,
            regex = /[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}/i;

        // min length 6, max length 8, matches postcode regex
        if (postcode.length >= 6 && postcode.length <= 8 && regex.test(postcode)) { return true; } else {
            return { message: 'Please enter a valid postcode' }
        }
    }

}
*/
