import Vue from 'vue'
import Router from 'vue-router'
import boilerplateIndex from '@/components/boilerplate-index'
import boilerplateitem from '@/components/boilerplate-item'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: boilerplateIndex
    }
  ]
})
